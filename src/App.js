import React from 'react';
import './App.css';
import HomePage from './pages/homepage/homepage.component';
import NavigationBar from './components/navigation-bar/navigation-bar.component'
import Footer from './components/footer/footer.component';

function App() {
  return (
    <div>
      <NavigationBar />
      <HomePage />
      <Footer />
    </div>
  );
}

export default App;
