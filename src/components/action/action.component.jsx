import React from 'react';
import './action.styles.scss';
import Col from 'react-bootstrap/Col';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import Buy from '../buy/buy.component';

const Action = () => {

    return(
        <Col>
            <div className="title-handle">
                <img src={require('./images/tickets.png')} alt="tickets"/>
                <h1>Azioni</h1>
            </div>
            <Tabs className="menu-section" defaultActiveKey="buy" id="menu-down">
                <Tab className="menu-options" eventKey="launch" title="ANNUNCI">
                    <h1>annunci</h1>
                    <div className="arrow-up"></div>
                </Tab>
                <Tab className="menu-options" eventKey="charge" title="CARICA CONTO">
                    <h1>carica conto</h1>
                    <div className="arrow-up"></div>
                </Tab>
                <Tab className="menu-options" eventKey="settings" title="Configura">
                    <h1>configura</h1>
                    <div className="arrow-up"></div>
                </Tab>
                <Tab className="menu-options" eventKey="code" title="USA CODICE">
                    <h1>Usa codice</h1>
                    <div className="arrow-up"></div>
                </Tab>
                <Tab className="menu-options" eventKey="buy" title="ACQUISTA">
                    <Buy />
                    <div className="arrow-up"></div>
                </Tab>
                <Tab className="menu-options" eventKey="transactions" title="Transazioni">
                    <h1>Transazioni</h1>
                    <div className="arrow-up"></div>
                </Tab>
            </Tabs>
        </Col>
    )
}

export default Action;