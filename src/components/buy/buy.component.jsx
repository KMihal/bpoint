import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Accordion from 'react-bootstrap/Accordion';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import './buy.styles.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown, faCircleNotch } from '@fortawesome/free-solid-svg-icons';

const Buy = () => {
    return(
        <div className="buy-section">
            <h1>acquista</h1>
            <Row>
                <Col md={8}>
                    <Accordion>
                        <Card>
                            <Card.Header>
                                <div className="dot-text"></div>
                                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                    Prodotti Postali
                                </Accordion.Toggle>
                                <Accordion.Toggle className="ArrowDown" as={Button} variant="link" eventKey="0">
                                   <FontAwesomeIcon icon={faAngleDown} />
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>Prodotti Postali</Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Card.Header>
                                <div className="dot-text"></div>
                                <Accordion.Toggle as={Button} variant="link" eventKey="1">
                                    ricariche telefoniche dirette
                                </Accordion.Toggle>
                                <Accordion.Toggle className="ArrowDown" as={Button} variant="link" eventKey="1">
                                   <FontAwesomeIcon icon={faAngleDown} />
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="1">
                                <Card.Body>ricariche telefoniche dirette</Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Card.Header>
                                <div className="dot-text"></div>
                                <Accordion.Toggle as={Button} variant="link" eventKey="2">
                                ricariche telefoniche con codice pin
                                </Accordion.Toggle>
                                <Accordion.Toggle className="ArrowDown" as={Button} variant="link" eventKey="1">
                                   <FontAwesomeIcon icon={faAngleDown} />
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="2">
                                <Card.Body>ricariche telefoniche con codice pin</Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Card.Header>
                                <div className="dot-text"></div>
                                <Accordion.Toggle as={Button} variant="link" eventKey="3">
                                ricariche telefoniche internazionali
                                </Accordion.Toggle>
                                <Accordion.Toggle className="ArrowDown" as={Button} variant="link" eventKey="3">
                                   <FontAwesomeIcon icon={faAngleDown} />
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="3">
                                <Card.Body>ricariche telefoniche internazionali</Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Card.Header>
                                <div className="dot-text"></div>
                                <Accordion.Toggle as={Button} variant="link" eventKey="4">
                                carte di credito
                                </Accordion.Toggle>
                                <Accordion.Toggle className="ArrowDown" as={Button} variant="link" eventKey="4">
                                   <FontAwesomeIcon icon={faAngleDown} />
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="4">
                                <Card.Body>carte di credito</Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Card.Header>
                                <div className="dot-text"></div>
                                <Accordion.Toggle as={Button} variant="link" eventKey="5">
                                ricariche scommesse sportive
                                </Accordion.Toggle>
                                <Accordion.Toggle className="ArrowDown" as={Button} variant="link" eventKey="5">
                                   <FontAwesomeIcon icon={faAngleDown} />
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="5">
                                <Card.Body>ricariche scommesse sportive</Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Card.Header>
                                <div className="dot-text"></div>
                                <Accordion.Toggle as={Button} variant="link" eventKey="6">
                                ricariche televisioni digitali
                                </Accordion.Toggle>
                                <Accordion.Toggle className="ArrowDown" as={Button} variant="link" eventKey="6">
                                   <FontAwesomeIcon icon={faAngleDown} />
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="6">
                                <Card.Body>ricariche televisioni digitali</Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Card.Header>
                                <div className="dot-text"></div>
                                <Accordion.Toggle as={Button} variant="link" eventKey="7">
                                gift card
                                </Accordion.Toggle>
                                <Accordion.Toggle className="ArrowDown" as={Button} variant="link" eventKey="7">
                                   <FontAwesomeIcon icon={faAngleDown} />
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="7">
                                <Card.Body>gift card</Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Card.Header>
                                <div className="dot-text"></div>
                                <Accordion.Toggle as={Button} variant="link" eventKey="8">
                                crypto valute
                                </Accordion.Toggle>
                                <Accordion.Toggle className="ArrowDown" as={Button} variant="link" eventKey="8">
                                   <FontAwesomeIcon icon={faAngleDown} />
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="8">
                                <Card.Body>crypto valute</Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Card.Header>
                                <div className="dot-text"></div>
                                <Accordion.Toggle as={Button} variant="link" eventKey="9">
                                biglietteria
                                </Accordion.Toggle>
                                <Accordion.Toggle className="ArrowDown" as={Button} variant="link" eventKey="9">
                                   <FontAwesomeIcon icon={faAngleDown} />
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="9">
                                <Card.Body>biglietteria</Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Card.Header>
                                <div className="dot-text"></div>
                                <Accordion.Toggle as={Button} variant="link" eventKey="10">
                                Acquisti Online
                                </Accordion.Toggle>
                                <Accordion.Toggle className="ArrowDown" as={Button} variant="link" eventKey="10">
                                   <FontAwesomeIcon icon={faAngleDown} />
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="10">
                                <Card.Body>Acquisti Online</Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                </Col>
                <Col md={4}>
                    <Button className="click-the-service">
                        <img src={require('./images/click.png')} alt=""/>
                        Seleziona il servizio
                    </Button> 
                </Col>
            </Row>
        </div>
    )
} 

export default Buy;