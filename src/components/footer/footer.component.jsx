import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Button from 'react-bootstrap/Button';
import './footer.styles.scss';

const Footer = () => {

    return(
        <div>
            <Container className="fluid-width">
            <Container>
                <Row className="footer-handler">
                    <Col md={10} className="text-section">
                        <p>Copyright © 2013-2019 BPOINT Company. All rights reserved.</p>
                    </Col>
                    <Col md={2} className="button-section">
                        <ButtonGroup>
                            <Button>
                                <img src={require('./images/facebook.png')} alt="Facebook"/>
                            </Button>
                            <Button>
                                <img src={require('./images/Instagram.png')} alt="Instagram"/>
                            </Button>
                            <Button>
                                <img src={require('./images/linkedin.png')} alt="Linkedin"/>
                            </Button>
                        </ButtonGroup>
                    </Col>
                </Row>
            </Container>
            </Container>
        </div>
    )
}

export default Footer