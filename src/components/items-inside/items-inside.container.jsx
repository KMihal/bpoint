import React from 'react';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTag, faUser, faChartLine } from '@fortawesome/free-solid-svg-icons';
import './items-inside.styles.scss'


const ItemsInside = () => {

    return(
        <Row>
            <Col className="blue-box">
                <div className="box-holder">
                    <img src={require('./images/statistic-blue.png')} alt="statistic"/>
                    <Button>
                        View Detail
                    </Button>
                    <p>//today</p>
                    <h2>Overall Sale</h2>
                    <h1>1234€</h1>
                    <FontAwesomeIcon className="icon-position" icon={faTag} />
                </div>
            </Col>
            <Col className="orange-box">
                <div className="box-holder">
                    <img src={require('./images/statistic-blue.png')} alt="statistic"/>
                    <Button>
                        View Detail
                    </Button>
                    <p>//today</p>
                    <h2>Overall Sale</h2>
                    <h1>1446</h1>
                    <FontAwesomeIcon className="icon-position" icon={faUser} />
                </div>
            </Col>
            <Col className="green-box">
                <div className="box-holder">
                    <img src={require('./images/statistic-blue.png')} alt="statistic"/>
                    <Button>
                        View Detail
                    </Button>
                    <p>//today</p>
                    <h2>Overall Sale</h2>
                    <h1>65%</h1>
                    <FontAwesomeIcon className="icon-position" icon={faChartLine} />
                </div>
            </Col>
            <div className="filters">
                <div className="Today-box">
                    <p>Today</p>
                </div>
                <div className="Month-box">
                    <p>Month</p>
                </div>
                <div className="Year-box">
                    <p>Year</p>
                </div>
            </div>
            <div className="download-filters">
                <Button className="download-btn">
                    <img src={require('./images/Download-icon.png')} alt="Download"/>
                </Button>
                <Button className="filter-btn">
                    Select filter
                    <img src={require('./images/filter-icon.png')} alt="Filter"/>
                </Button>
            </div>
        </Row>
    )
}

export default ItemsInside;