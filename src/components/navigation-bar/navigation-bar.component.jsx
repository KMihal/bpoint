import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { faBell, faEnvelope, faCog, faSearch } from '@fortawesome/free-solid-svg-icons';
import Badge from 'react-bootstrap/Badge';
import './navigation-bar.styles.scss';
import Container from 'react-bootstrap/Container'

const NavigationBar = () => {

    return(
        <Navbar bg="white" variant="light" className="navigation-container">
            <Navbar.Brand href="#home" className="banner-content">
                <img src={require('./images/Logo.png')} alt="Logo"/>
            </Navbar.Brand>
            <Container>
            <Nav className="navigation-item">
                <p>Credito: <span> 2.400 €</span></p>
                <Form>
                    <FormControl type="text" placeholder="Search here..." className="mr-sm-2" />
                    <FontAwesomeIcon icon={faSearch}  className="search-bar"/>
                </Form>
                <Nav.Link href="#features">MENU 2</Nav.Link>
                <Nav.Link href="#pricing">MENU 1</Nav.Link>
                <div className="Buttons-holder">
                    <Button>
                        <FontAwesomeIcon icon={faBell} />
                        <Badge className="badge-style">8</Badge>
                    </Button>
                    <Button>
                        <FontAwesomeIcon icon={faEnvelope} />
                        <Badge className="badge-style">5</Badge>
                    </Button>
                    <Button>
                        <FontAwesomeIcon icon={faCog} />
                    </Button>
                </div>
                <div className="border-photo">
                    <img src={require('./images/Profile.png')} alt="profile"/>
                </div>
                <p className="name">Mario Rossi</p>
                <NavDropdown className="arrow-down" title="" id="collasible-nav-dropdown">
                    <NavDropdown.Item>Log out</NavDropdown.Item>
                </NavDropdown>
            </Nav>
            </Container>
        </Navbar>
    )
}

export default NavigationBar;