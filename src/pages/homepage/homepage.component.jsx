import React, {useState} from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Fade from 'react-bootstrap/Fade';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronUp } from '@fortawesome/free-solid-svg-icons';
import './homepage.styles.scss';
import ItemsInside from '../../components/items-inside/items-inside.container';
import Action from '../../components/action/action.component';


const HomePage = () => {

    const [open, setOpen] = useState(true);

    return(
        <div className="background-changer">
            <Container>
                <Row>
                    <Col className="overview">
                        <Button
                            className="open-close"
                            onClick={() => setOpen(!open)}
                            aria-controls='First-collapse'
                            aria-expanded={open}
                        >
                            <img className="first-icon" src={require('./images/Km.png')} alt="Km"/>
                            Overview 
                            <FontAwesomeIcon className="arrow-up-down" icon={faChevronUp} />
                        </Button>
                        <Fade in={open} className="collapse-section">
                            <div id="First-collapse">
                                <ItemsInside />
                            </div>
                        </Fade>
                    </Col>
                </Row>
            </Container>
            <Container>
                <Row>
                    <Action />
                </Row>
            </Container>
        </div>
    )
}

export default HomePage;